﻿using System;

namespace GenericosRestricciones
{
    class Program
    {
        static void Main(string[] args)
        {
            AlmacenEmpleados<Estudiante> empleados = new AlmacenEmpleados<Estudiante>(3);

            empleados.agregar(new Estudiante(2500));
            empleados.agregar(new Estudiante(3100));
            empleados.agregar(new Estudiante(4500));

        }

        class AlmacenEmpleados<T> where T:IParaEmpleados
        {
            private int i = 0;

            private T[] datosEmpleado;

            public AlmacenEmpleados(int z)
            {
                this.datosEmpleado = new T[z];
            }

            public void agregar(T obj)
            {
                this.datosEmpleado[i] = obj;
                this.i++;
            }

            public T getEmpleado(int pos)
            {
                return this.datosEmpleado[pos];
            }
        }

        class Director: IParaEmpleados
        {
            private double _salario;
            public Director(double salario)
            {
                this._salario = salario;
            }

            public double getSalario()
            {
                return this._salario;
            }

        }

        class Secretaria
        {
            private double _salario;
            public Secretaria(double salario)
            {
                this._salario = salario;
            }

            public double getSalario()
            {
                return this._salario;
            }
        }

        class Electricista
        {
            private double _salario;
            public Electricista(double salario)
            {
                this._salario = salario;
            }

            public double getSalario()
            {
                return this._salario;
            }
        }

        class Estudiante
        {
            private int _edad;
            public Estudiante(int edad)
            {
                this._edad = edad;
            }
        }

        interface IParaEmpleados
        {
            double getSalario();
        }

    }
}

