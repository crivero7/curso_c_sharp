﻿using System;

namespace Destructores
{
    class Program
    {
        static void Main(string[] args)
        {
            ManejoArchivos miArchivo = new ManejoArchivos();
            miArchivo.mensaje();
        }

        class ManejoArchivos
        {
            StreamReader archivo;
            int contador = 0;
            string linea;

            public ManejoArchivos()
            {
                archivo = new StreamReader(@"archivo.txt");

                while((linea = archivo.ReadLine()) != null)
                {
                    System.Console.WriteLine(linea);

                    this.contador++;
                }
            }

            public void mensaje()
            {
                System.Console.WriteLine($"Hay {this.contador} lineas");
            }

            ~ManejoArchivos()
            {
                this.archivo.Close();
            }
        }
    }
}

