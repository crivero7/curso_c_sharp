﻿using System;

namespace dotnetcore
{
    class Punto
    {
        double x, y;
        private static int contadorDeObjetos = 0;
        public Punto()
        {
            this.x = 0.0;
            this.y = 0.0;
            contadorDeObjetos++;
        }

        public Punto(double x, double y)
        {
            this.x = x;
            this.y = y;
            contadorDeObjetos++;
        }

        public double DistanciaHacia(Punto otroPunto)
        {
            double xDiff = this.x - otroPunto.x;
            double yDiff = this.y - otroPunto.y;

            double distancia = Math.Sqrt(Math.Pow(xDiff, 2) + Math.Pow(yDiff, 2));

            return distancia;
        }

        public static int getContadorDeobjetos() {
            return contadorDeObjetos;

        }
    }
}
