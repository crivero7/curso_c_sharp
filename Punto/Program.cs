﻿using System;


namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            realizarTarea();
            
        }

        static void realizarTarea()
        {
            Punto origen = new Punto();
            Punto destino = new Punto(150, 90);
            double distancia = origen.DistanciaHacia(destino);
            Console.WriteLine($"La distancia entre los puntos es: {distancia}");
            Console.WriteLine($"Número de objetos creados: {Punto.getContadorDeobjetos()}");
        }
    }
}
