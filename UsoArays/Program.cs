﻿using System;

namespace UsoArrays
{
    class Program
    {
        static void Main(String[] args)
        {
            int[] numeros = new int[4];
            numeros[0] = 7;
            numeros[1] = 9;
            numeros[2] = 15;
            numeros[3] = 3;

            ProcesaDatos(numeros);
            foreach(int i in numeros)
            {
                System.Console.WriteLine(i);
            }

            int[] datos = LeerDatos();

            foreach(int dato in datos)
            {
                System.Console.WriteLine(dato);
            }
        }

        static void ProcesaDatos(int[] datos)
        {
            for (int i=0; i<datos.Length; i++)
            {
                datos[i] += 10;
            }

            
        }
        static int[] LeerDatos()
        {
            System.Console.WriteLine("¿Cuántos Elementos quiere que tenga el Array?");
            string respuesta = Console.ReadLine();
            int numElementos = int.Parse(respuesta);
            int[] datos = new int[numElementos];

            for(int i=0; i<numElementos; i++)
            {
                System.Console.WriteLine($"Introduce el datos para la posición: {i}");

                respuesta = Console.ReadLine();

                int datoElementos = int.Parse(respuesta);

                datos[i] = datoElementos;
            }

            return datos;
        }

    }
}

