﻿using System;

namespace Properties
{
    class Program
    {
        public static void Main(string[] args)
        {
           Empleado Juan = new Empleado("Juan") ;

           Juan.SALARIO = 1200;

           Juan.SALARIO += 500;

           System.Console.WriteLine($"El salario de {Juan.getNombre()} es: {Juan.SALARIO}");
        }

        class Empleado
        {
            private string _nombre;
            private double _salario;
            public Empleado(string nombre)
            {
                this._nombre = nombre;
            }

            private double evaluarSalario(double salario)
            {
                if (salario < 0) return 0;
                else return salario;
            }

            // // Creación de propiedad
            // public double SALARIO
            // {
            //     get { return this.salario; }
            //     set { this.salario = this.evaluarSalario(value); }
            // }

            public double SALARIO
            {
                get => this._salario;
                set => this._salario = this.evaluarSalario(value);
            }
            public string getNombre()
            {
                return this._nombre;
            }

        }
    }
}
