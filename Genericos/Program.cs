﻿using System;

namespace Genericos
{
    class Program
    {
        static void Main(string[] args)
        {
            AlmacenObjetos<Empleado> archivos = new AlmacenObjetos<Empleado>(4);

            // archivos.agregar("Carlos");
            // archivos.agregar("Daniel");
            // archivos.agregar("Rivero");
            // archivos.agregar("Ramírez");

            // string persona0 = (string) archivos.getElemento(0);

            archivos.agregar(new Empleado(2500.5));
            archivos.agregar(new Empleado(3150.5));
            archivos.agregar(new Empleado(758.6));

            Empleado persona0 = archivos.getElemento(0);

            System.Console.WriteLine($"El salario del empleado es: {persona0.SALARIO}");
        }
    }

    class AlmacenObjetos<T>
    {
        private T[] datosElemento;
        private int i = 0;
        public AlmacenObjetos(int z)
        {
            this.datosElemento = new T[z];
        }

        public void agregar(T obj)
        {
            this.datosElemento[this.i] = obj;
            this.i++;
        }

        public T getElemento(int pos)
        {
            return this.datosElemento[pos];
        }
    }

    class Empleado
    {
        private double _salario;

        public Empleado(double salario)
        {
            this._salario = salario;
        }

        public double SALARIO
        {
            get => this._salario;
        }
    }
}

