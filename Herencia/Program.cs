﻿using System;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Caballo babieca = new Caballo("Corazon loco");
            Humano juan = new Humano("Carlos");
            Gorila copito = new Gorila("Tarzan");

            babieca.getNombre();

            Lagartija tronchatoro = new Lagartija("Tronchatoro");

            tronchatoro.getNombre();

            babieca.galopar();
        }

        abstract class Animales
        {
            public void respirar()
            {
                System.Console.WriteLine("Soy capaz de respirar");
            }

            public abstract void getNombre();
        }

        class Lagartija : Animales
        {
            string nombreReptil;

            public Lagartija(string nombre)
            {
                this.nombreReptil = nombre;
            }
            public override void getNombre()
            {
                System.Console.WriteLine($"El nombre del reptil es: {nombreReptil}");
            }
        }

        class Mamiferos : Animales
        {
            string nombreSerVivo;
            public Mamiferos(string nombre)
            {
                this.nombreSerVivo = nombre;
            }
            public void cuidarCrias()
            {
                System.Console.WriteLine("Cuido de mis crías hasta que se valgan por si solas");
            }

            public override void getNombre()
            {
                System.Console.WriteLine($"El nombre del mamífero es: {this.nombreSerVivo}");
            }
        }

        class Caballo : Mamiferos
        {
            public Caballo(string nombre) :base(nombre)
            {

            }
            public void galopar()
            {
                System.Console.WriteLine("Soy capaz de galopar");
            }
        }

        class Humano : Mamiferos
        {
            public Humano(string nombre) :base(nombre)
            {

            }
            public void pensar()
            {
                System.Console.WriteLine("Soy capaz de pensar ¿?");
            }
        }

        class Gorila : Mamiferos
        {
            public Gorila(string nombre) :base(nombre)
            {

            }
            public void trepar()
            {
                System.Console.WriteLine("Soy capaz de trepar");
            }
        }
    }
}
