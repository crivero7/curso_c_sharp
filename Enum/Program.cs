﻿using System;

namespace Enumerations
{
    enum Bonus { Bajo=500, Normal=1000, Bueno=1500, Excelente=3000}

    class Program
    {
        static void Main(string[] args)
        {
            Empleado Carlos = new Empleado(Bonus.Bueno, 1250.5);
            System.Console.WriteLine($"El salario del empleado es: ${Carlos.SALARIO}");
        }
    }

    class Empleado
    {
        private double _salario, _bonus;

        public Empleado(Bonus bonusEmpleado, double salarioBase)
        {
            this._bonus = (double) bonusEmpleado;   
            this._salario = salarioBase;
        }

        public double SALARIO
        {
            get => this._salario + this._bonus;
        }
    }
}

